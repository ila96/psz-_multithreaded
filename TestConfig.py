import json
from ConfigManager import ConfigManager

def test_load_dictionary():
    with open('config.json', 'r') as config:
        s = config.read()
        d = json.loads(s)
        urls = d['URLS_TO_GET_ADS'] 
        print(urls)

        assert(type(urls) == type({'a': 'a'}))

def test_should_stop_producer():
    config = ConfigManager.get_config_manager()
    id = 1
    should_stop_dict = config.get_config('SHOULD_STOP_PRODUCER')
    print(should_stop_dict[id])

test_load_dictionary()
test_should_stop_producer()