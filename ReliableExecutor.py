import sys
import threading
from ProxyManager import ProxyManager
from ConfigManager import ConfigManager
import urllib3
import logging
from datetime import datetime
from urllib3.util.retry import Retry
from urllib3 import ProxyManager as UrlLibProxyManager

class ReliableExecutor(threading.Thread):

    s_proxy_manager = None
    s_config_manager = None
    s_ad_pool = []
    s_current_read_ad = 0
    s_debug_logger = None
    s_lock = threading.Lock()
    
    STARTING_REQUEST_MESSAGE_PATTERN = "Starting request:{0},proxy:{1}"
    FINISHED_REQUEST_MESSAGE_PATTERN = "Finished request:{0},proxy:{1}"
    TRIM_AD_POOL_MESSAGE_PATTERN = "Ad pool is being trimmed at point:{0}. New length:{1}"

    def __init__(self, id, worker_type):
        threading.Thread.__init__(self)
        self.id = id
        self.type = worker_type
        ReliableExecutor.s_proxy_manager = ProxyManager.get_proxy_manager()
        ReliableExecutor.s_config_manager = ConfigManager.get_config_manager()
        self.current_proxy = ''
        ReliableExecutor.s_debug_logger = ReliableExecutor.create_file_logger('debug_log', logging.DEBUG, 'debug.log')

    @staticmethod
    def create_file_logger(name, level, file):
        if ReliableExecutor.s_debug_logger is None:
            logger = logging.getLogger(name)
            logger.setLevel(level)
            #fh = logging.FileHandler(file)
            #fh.setLevel(level)
            #logger.addHandler(fh)
            return logger

        return ReliableExecutor.s_debug_logger

    def log_message_with_timestamp(self, message):
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        timestamped_message = current_time + ',' + message + ',' + self.type + ':' + str(self.id)
        ReliableExecutor.s_debug_logger.log(logging.DEBUG, timestamped_message)

    def get_next_ad_url(self):
        ReliableExecutor.s_lock.acquire()

        ad_url = None
        
        if len(ReliableExecutor.s_ad_pool) > ReliableExecutor.s_current_read_ad:
            ad_url = ReliableExecutor.s_ad_pool[ReliableExecutor.s_current_read_ad]
            ReliableExecutor.s_current_read_ad += 1

        if ReliableExecutor.s_current_read_ad > 1000:
            ReliableExecutor.s_ad_pool = ReliableExecutor.s_ad_pool[ReliableExecutor.s_current_read_ad:]
            
            self.log_message_with_timestamp(ReliableExecutor.TRIM_AD_POOL_MESSAGE_PATTERN.format(ReliableExecutor.s_current_read_ad, len(ReliableExecutor.s_ad_pool)))

            ReliableExecutor.s_current_read_ad = 0
            

        ReliableExecutor.s_lock.release()

        return ad_url

    def insert_ads(self, ad_urls):
        ReliableExecutor.s_lock.acquire()

        for ad_url in ad_urls:
            ReliableExecutor.s_ad_pool.append(ad_url)
        
        ReliableExecutor.s_lock.release()

        self.log_message_with_timestamp("Ad pool length:{0}".format(len(ReliableExecutor.s_ad_pool)))

    def execute(self, url):
        failure_count = ReliableExecutor.s_config_manager.get_config('HTTPS_REQUEST_FAILURE_COUNT')

        for i in range(failure_count):
            try:
                self.log_message_with_timestamp('Getting proxy')
                self.current_proxy = ReliableExecutor.s_proxy_manager.get_proxy()
                self.log_message_with_timestamp('Got proxy')
                
                proxy_url = 'http://' + self.current_proxy
                default_headers = {'User-Agent' : 'Chrome'}
                proxy_manager = UrlLibProxyManager(proxy_url, proxy_headers=default_headers)
                timeout = ReliableExecutor.s_config_manager.get_config('HTTPS_REQUEST_TIMEOUT')

                self.log_message_with_timestamp(ReliableExecutor.STARTING_REQUEST_MESSAGE_PATTERN.format(url, self.current_proxy))
                response = proxy_manager.request('GET', url, timeout=timeout, retries=Retry(0)).data.decode('utf-8')
                self.log_message_with_timestamp(ReliableExecutor.FINISHED_REQUEST_MESSAGE_PATTERN.format(url, self.current_proxy))

                return response
            except Exception as e:
                self.log_message_with_timestamp(str(e))

                if 'relative_url' in str(e):
                    exc_type, exc_value, exc_traceback = sys.exc_info()

                    self.log_message_with_timestamp(exc_type)
                    self.log_message_with_timestamp(exc_value)
                    self.log_message_with_timestamp(exc_traceback)

                ReliableExecutor.s_proxy_manager.chill_proxy(self.current_proxy)

        return None