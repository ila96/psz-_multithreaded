import threading

def test():
    print('jeej')

class TestBarrier(threading.Thread):

    barrier = threading.Barrier(2, action=test)

    def run(self):
        print('Before first barrier')
        TestBarrier.barrier.wait()
        print('Before second barrier')
        TestBarrier.barrier.wait()
        print('Passed second barrier')


t1 = TestBarrier()
t2 = TestBarrier()

t1.start()
t2.start()

