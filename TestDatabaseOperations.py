from DatabaseOperations import DatabaseOperations
import mysql.connector
import logging

def test_multiple_connections():
    try:
        c1 = mysql.connector.connect(user='psz',
                                        password='psz',
                                        host='127.0.0.1',
                                        database='psz')

        c2 = mysql.connector.connect(user='psz',
                                        password='psz',
                                        host='127.0.0.1',
                                        database='psz')

        c3 = mysql.connector.connect(user='psz',
                                        password='psz',
                                        host='127.0.0.1',
                                        database='psz')

        c1.close()
        c2.close()
        c3.close()
    except:
        assert(1 == 0)

def test_mulitple_connections_write():
    d1 = DatabaseOperations()
    d2 = DatabaseOperations()

    #def insert_into_database(self, command, parameters):
    query = "INSERT INTO `psz`.`test_table` (`text`) VALUES ('neca kralj');"
    d1.insert_into_database(query, {})

    d2.insert_into_database(query, {})

def test_insert_ad_url():
    d1 = DatabaseOperations()
    #def insert_using_template_command(self, command_name, parameters):
    d1.insert_using_template_command('insert_ad_url', {'ad_url': 'nesto nesto2', 'extracted': False})

def test_select_ad_urls():
    logger = logging.getLogger('simple_example')
    logger.setLevel(logging.DEBUG)
    d1 = DatabaseOperations(logger)

test_multiple_connections()

test_mulitple_connections_write()
test_insert_ad_url()