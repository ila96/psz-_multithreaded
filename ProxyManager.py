from datetime import datetime
from itertools import cycle
import logging
import requests
import threading
from ConfigManager import ConfigManager
from lxml.html import fromstring
import time

class ProxyManager:

    s_proxy_manager = None

    def __init__(self):
        self.config_manager = ConfigManager.get_config_manager()
        self.cold_proxies = {}
        self.lock = threading.Lock()
        self.last_refresh = None
        self.proxies = set()
        self.__initialize_proxy_pool()

    @staticmethod
    def get_proxy_manager():
        if ProxyManager.s_proxy_manager is None:
            ProxyManager.s_proxy_manager = ProxyManager()

        return ProxyManager.s_proxy_manager

    def __initialize_proxy_pool(self):
        self.last_refresh = datetime.now()

        if self.config_manager.get_config('CLEAR_BANNED_PROXIES'):
            self.cold_proxies = {}
        
        self.__get_proxies_from_free_proxy_list()
        self.__get_proxies_from_file()

        self.proxy_pool = cycle(self.proxies)
        self.current_proxy = None
        
        if len(self.proxies) == 0:
            # sleep 5 minutes and then reinitialize proxy pool
            time.sleep(5 * 60)
            self.get_proxies()

        #self.REINITIALIZE_COUNT_LIMIT = len(self.proxies)

        return self.proxies

    def refresh(self, force=False):
        ### Must access config without lock to prevent deadlock
        if force or ((datetime.now() - self.last_refresh).total_seconds() / 60) > self.config_manager.get_config_read('PROXY_REFRESH_TIME_IN_MINUTES'):
            self.__initialize_proxy_pool()

    def get_proxy(self):
        self.lock.acquire()

        proxy = self.__get_first_available_proxy()

        self.lock.release()

        return proxy

    def reset_cold_proxies(self):
        self.cold_proxies = {}

    def __get_first_available_proxy(self):
        self.refresh()

        self.current_proxy = next(self.proxy_pool)

        if len(self.proxies) == len(self.cold_proxies):
            self.reset_cold_proxies()
            self.refresh(force=True)

        while self.current_proxy in self.cold_proxies:
            self.current_proxy = next(self.proxy_pool)
        
        proxy = self.current_proxy
        return proxy

    def get_proxy_and_return_proxy(self, proxy):
        self.lock.acquire()

        self.cold_proxies[proxy] = datetime.now()
        proxy = self.__get_first_available_proxy()

        self.lock.release()

        return proxy

    def chill_proxy(self, proxy):
        self.lock.acquire()

        self.cold_proxies[proxy] = datetime.now()

        self.lock.release()

    def __get_proxies_from_file(self):
        with open('proxies.txt', 'r') as proxies_file:
            proxy_lines = proxies_file.readlines()

        for line in proxy_lines:
            if line not in self.cold_proxies and line not in self.proxies:
                self.proxies.add(line)
    
    def __get_proxies_from_free_proxy_list(self):
        try:
            url = 'https://free-proxy-list.net/'
            response = requests.get(url)
            parser = fromstring(response.text)
            
            for i in parser.xpath('//tbody/tr'):
                # Grabbing elite https proxies
                if i.xpath('.//td[7][contains(text(),"yes")]') and i.xpath('.//td[5][contains(text(), "elite")]'):
                    #Grabbing IP and corresponding PORT
                    proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
                    
                    if proxy not in self.cold_proxies:
                        self.proxies.add(proxy)
            
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            logging.log(logging.WARNING, "{0} | Got {1} proxies from free-proxy-list.net ".format(current_time, len(self.proxies)))
        except Exception as e:
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            logging.log(logging.WARNING, "{0} | Unable to get proxies from free-proxy-list.net ".format(current_time, len(self.proxies)))