from DatabaseOperations import DatabaseOperations
from ReliableExecutor import ReliableExecutor

def class LocalProducer(ReliableExecutor):

    def __init__(self, id):
        ReliableExecutor.__init__(self, id, 'producer')
        self.database = DatabaseOperations(ReliableExecutor.s_debug_logger)

    def run(self):
        urls = self.database.select_from_database('select_ad_urls', {})

        self.insert_ads(urls)