import time
import json
import re
from DatabaseOperations import DatabaseOperations
from ReliableExecutor import ReliableExecutor
import random

ATTRIBUTE_KEYS = {
    'CITY' : 'grad_s',
    'LOCATION' : 'lokacija_s',
    'MICROLOCATION' : 'mikrolokacija_s',
    'STREET' : 'ulica_t',
    'BUILT_YEAR' : 'built_year',
    'STREET_NUMBER' : 'ulica_s_unit_s',
    'REAL_ESTATE_TYPE' : 'tip_nekretnine_s',
    'AREA' : 'kvadratura_d',
    'AREA_UNIT' : 'kvadratura_d_unit_s',
    'NUMBER_OF_ROOMS' : 'broj_soba_s',
    'PRICE' : 'cena_d',
    'PRICE_UNIT' : 'cena_d_unit_s',
    'FLOOR' : 'sprat_s',
    'TOTAL_FLOORS' : 'sprat_od_s',
    'ADDITIONAL_INFO' : 'dodatno_ss',
    'HAS_PAPERS' : 'Uknjižen',
    'BACKYARD_AREA' : 'povrsina_placa_d',
    'BACKYARD_AREA_UNIT' : 'povrsina_placa_d_unit_s',
    'HEATING_SYSTEM' : 'grejanje_s'
}

SQL_COLUMNS = {
    'REAL_ESTATE_TYPE' : 'real_estate_type',
    'AD_TYPE' : 'ad_type',
    'CITY' : 'city',
    'LOCATION' : 'location',
    'MICROLOCATION' : 'microlocation',
    'STREET' : 'address',
    'BUILT_YEAR' : 'built_year',
    'AREA' : 'area',
    'AREA_UNIT': 'area_unit',
    'BACKYARD_AREA' : 'backyard_area',
    'BACKYARD_AREA_UNIT': 'backyard_area_unit',
    'FLOOR' : 'floor',
    'TOTAL_FLOORS' : 'total_floors',
    'HAS_PAPERS' : 'has_papers',
    'HEATING_SYSTEM' : 'heating_system',
    'NUMBER_OF_ROOMS' : 'number_of_rooms',
    'NUMBER_OF_BATHROOMS' : 'number_of_bathrooms',
    'RELATIVE_URL' : 'relative_url',
    'PRICE': 'price',
    'PRICE_UNIT': 'price_unit'
}


class Consumer(ReliableExecutor):

    BASE_URL = 'https://www.halooglasi.com'
    PATTERN = '"OtherFields":({[^;]*}),'

    def __init__(self, id):
        ReliableExecutor.__init__(self, id, 'consumer')
        self.database = DatabaseOperations(ReliableExecutor.s_debug_logger)

    def ad_exists(self, url):
        #def select_from_database(self, command_name, parameters):
        result = self.database.select_from_database('select_ad_with_relative_url', {'relative_url' : url})

        if result is not None and len(result) > 0:
            return True

        return False

    def extract_built_year(self, html_text):
        self.log_message_with_timestamp('Starting extraction of build year')
        m = re.search('godina izgradnje[ :]*([12][0-9]{3})', html_text, re.IGNORECASE)
        try:
            if m is not None:
                return m[1]
        except:
            self.log_message_with_timestamp('No information about year the place was built')

        return None

    def extract_ad_info(self, html_text):
        self.log_message_with_timestamp('Starting extraction of ad_info')
        info = re.search(Consumer.PATTERN, html_text)
        ad_info = None

        try:
            if info is not None:
                year_built = self.extract_built_year(html_text)
                ad_info = json.loads(info[1])
                ad_info[ATTRIBUTE_KEYS['BUILT_YEAR']] = year_built
                self.log_message_with_timestamp('Finished extraction of ad_info')
                return ad_info
            else:
                self.log_message_with_timestamp('Unable to extract ad_info')
                self.log_message_with_timestamp('HTML TEXT:\n{0}'.format(html_text))
                return None
        except Exception as e:
            self.log_message_with_timestamp("Can't parse json data")
            return None
        
        return ad_info
        # add add type and real estate type

    def key_remap(self, key_mapping, dictionary):
        """
        Convert dictionary to another dictionary with different keys. 
        Let
            d = {'a' : 1, 'b' : 2, 'c' : 3}
            key_mapping = {'a' : 'first', 'b' : 'second', 'c' : 'third'}
            
            The resulting dictionary will be: result = {'first' : 1, 'second' : 2, 'third' : 3}
        
        If dictionary doesn't contain entry for some key in key_mapping, the value in resulting dictionary for that key will be None

        # Arguments
            key_mapping: Dictionary with key mapping.
            dictionary: Dictionary with values
        
        # Returns
            A new dictionary with mapped keys.
        """
        result = {}
        
        for key, value in key_mapping.items():
            if key in dictionary:
                result[value] = dictionary[key]
            else:
                result[value] = None
        
        return result

    def prepare_params(self, ad_info, url):
        self.log_message_with_timestamp('Starting parameters prepare for:{0}'.format(url))
        if ATTRIBUTE_KEYS['STREET_NUMBER'] in ad_info:    
            ad_info[ATTRIBUTE_KEYS['STREET']] = ad_info[ATTRIBUTE_KEYS['STREET']] + ad_info[ATTRIBUTE_KEYS['STREET_NUMBER']]

        # prepare params - conver ad_info dictionary to another dictionary
        key_map = {value : SQL_COLUMNS[key] for key, value in ATTRIBUTE_KEYS.items() if key in SQL_COLUMNS}
        params = self.key_remap(key_map, ad_info)

        for value in SQL_COLUMNS.values():
            if value not in params:
                params[value] = None

        #handle the case when ad has additional info (e.g. uknjizen)
        if ATTRIBUTE_KEYS['ADDITIONAL_INFO'] in ad_info:
            has_papers = ATTRIBUTE_KEYS['HAS_PAPERS'] in ad_info[ATTRIBUTE_KEYS['ADDITIONAL_INFO']]
        else:
            has_papers = None
            
        params[SQL_COLUMNS['HAS_PAPERS']] = has_papers

        # prepend real_estate_type and ad_type to params dictionary
        ad_type, real_estate_type = url.split('/')[2].split('-')
        params[SQL_COLUMNS['REAL_ESTATE_TYPE']] = real_estate_type
        params[SQL_COLUMNS['AD_TYPE']] = ad_type

        # set relative url for ad
        params[SQL_COLUMNS['RELATIVE_URL']] = url

        self.log_message_with_timestamp('Finished parameters prepare for:{0}'.format(url))
        return params

    def get_ad_info(self):
        self.log_message_with_timestamp("Getting ad from ad pool")
        ad_url = self.get_next_ad_url()

        if ad_url is None:
            sleep_time = ReliableExecutor.s_config_manager.get_config('CONSUMER_SLEEP_TIME_IN_MINUTES') * 60
            self.log_message_with_timestamp('Sleeping for:{0} seconds as there are no ads in the pool'.format(sleep_time))
            time.sleep(sleep_time)
            return

        self.log_message_with_timestamp("Got ad from ad pool")

        if self.ad_exists(ad_url):
            self.log_message_with_timestamp("Skipping. Add already exists. Relative_url:{0}".format(ad_url))
            return

        #ad_info = self.execute_action(self.get_information_about_ad, {'ad_url': ResilientScrapper.BASE_URL + url, 'regex':self.PATTERN})
        self.log_message_with_timestamp("Fetched url:{0} from pool".format(ad_url))

        url = Consumer.BASE_URL + ad_url
        response_text = self.execute(url)

        if response_text is None:
            return
        
        ad_info = self.extract_ad_info(response_text)

        if ad_info is not None:
            params = self.prepare_params(ad_info, ad_url)

            self.log_message_with_timestamp('Starting insert for:{0}'.format(url))
            self.database.insert_using_template_command('insert_ad_info', params)
            self.log_message_with_timestamp('Finished insert for:{0}'.format(url))

        # sleep a little bit so we don't overload the server
        sleep_time = random.randint(2,10)
        self.log_message_with_timestamp('Sleeping for:{0} seconds'.format(sleep_time))
        time.sleep(sleep_time)

    def should_stop(self):
        should_stop = ReliableExecutor.s_config_manager.get_config('SHOULD_STOP')
        should_stop_id_override = ReliableExecutor.s_config_manager.get_config('SHOULD_STOP_CONSUMER')[str(self.id)]

        if should_stop_id_override is not None:
            return should_stop_id_override
        
        return should_stop

    def run(self):
        should_stop = ReliableExecutor.s_config_manager.get_config('SHOULD_STOP_CONSUMER')[str(self.id)]

        while not should_stop:
            self.get_ad_info()
            should_stop = self.should_stop()

        self.log_message_with_timestamp('Stopping')