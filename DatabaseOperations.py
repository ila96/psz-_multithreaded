import os
import logging
import mysql.connector

class DatabaseOperations():

    def __init__(self, logger):
        self.connection = mysql.connector.connect(user='psz',
                                    password='psz',
                                    host='127.0.0.1',
                                    database='psz')
        self.logger = logger

    def insert_using_template_command(self, command_name, parameters):
        """
        Insert data into database using template insert statements from template folder

        # Arguments
            connection: MySql connection over which insert statement is executed
            command_name: name of file with insert statement
            parameters: list of parameters for above insert statement. Number of parameters in insert statement 
            must match number of parameters in insert statement

        # Returns
            Returns True if insert was successful otherwise False
        """
        try:
            with open(os.path.join('sql', command_name + '.sql'), mode='r') as f:
                command = f.read()
                self.insert_into_database(command, parameters)
                return True
        except Exception as e:
            self.logger.log(logging.DEBUG, e.args)

        return False

    
    def insert_into_database(self, command, parameters):
        """
        Insert data into database using command insert statement and parameters

        # Arguments
            connection: MySql connection over which insert statement is executed
            command: insert statement
            parameters: list or dictionary of parameters for above insert statement. Number of parameters in insert statement 
            must match number of parameters in 'parameters'
            'add_employee = ("INSERT INTO employees "
                "(first_name, last_name, hire_date, gender, birth_date) "
                "VALUES (%s, %s, %s, %s, %s)")'
        """
        cursor = self.connection.cursor()
        try:
            cursor.execute(command, parameters)
            self.connection.commit()
        except Exception as e:
            self.logger.log(logging.DEBUG, e.args)
        
        cursor.close()

    def select_from_database(self, command_name, parameters):
        command = ''

        try:
            with open(os.path.join('sql', command_name + '.sql'), mode='r') as f:
                command = f.read()
            
            cursor = self.connection.cursor()
            cursor.execute(command, parameters)
            result = cursor.fetchall()
            return result
        except Exception as e:
            self.logger.log(logging.DEBUG, e.args)

        return None