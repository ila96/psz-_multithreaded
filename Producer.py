from bs4 import BeautifulSoup
from DatabaseOperations import DatabaseOperations
from ReliableExecutor import ReliableExecutor
import time
import threading
import random
from ConfigManager import ConfigManager

class Producer(ReliableExecutor):

    s_page = -2
    s_page_lock = threading.Lock()
    s_barrier = None

    @staticmethod
    def init_barriers():
        """
        Must call this method before instantiating objects of this class
        """
        if Producer.s_barrier is None:
            Producer.s_barrier = threading.Barrier(parties=ConfigManager.get_config_manager().get_config('NUMBER_OF_PRODUCERS'))

    def __init__(self, id):
        ReliableExecutor.__init__(self, id, 'producer')
        Producer.init_barriers()
        self.page_limit = ReliableExecutor.s_config_manager.get_config('PRODUCER_PAGE_LIMIT')
        self.urls = ReliableExecutor.s_config_manager.get_config('URLS_TO_GET_ADS')
        self.current_ad_base_url_key = ReliableExecutor.s_config_manager.get_config('CURRENT_AD_BASE_URL_KEY')
        self.database = DatabaseOperations(ReliableExecutor.s_debug_logger)
        self.current_page = ReliableExecutor.s_config_manager.get_config('CURRENT_PAGE')

        Producer.s_page_lock.acquire()
        if Producer.s_page == -2:
            self.current_page = ReliableExecutor.s_config_manager.get_config('CURRENT_PAGE')
            Producer.s_page = self.current_page
        Producer.s_page_lock.release()

    @staticmethod
    def read_page_from_config():
        Producer.s_page = ReliableExecutor.s_config_manager.get_config('CURRENT_PAGE')

    @staticmethod
    def reset_page_number():
        Producer.s_page_lock.acquire()

        Producer.s_page = 0

        Producer.s_page_lock.release()

    def last_saved_progress(self):
        raise NotImplementedError

    def get_page_number(self):
        page_limit = ReliableExecutor.s_config_manager.get_config('PRODUCER_PAGE_LIMIT')

        Producer.s_page_lock.acquire()
        page = Producer.s_page
        Producer.s_page = Producer.s_page + 1

        Producer.s_page_lock.release()

        return page

    def extract_urls(self, html_text):
        try:
            self.log_message_with_timestamp('Starting extracting urls')
            page_content = BeautifulSoup(html_text, "html.parser")
            a = page_content.find_all('h3', 'product-title')
            links_soup = BeautifulSoup(str(a))
            a_class = links_soup.find_all('a')

            links = []
            for link_item in a_class:
                links.append(link_item['href'])

            self.log_message_with_timestamp('Finished extracting urls')
            return links
        except Exception as e:
            self.log_message_with_timestamp('Error extracting urls')
            self.log_message_with_timestamp(e.args)

        return None

    def should_stop(self):
        should_stop = ReliableExecutor.s_config_manager.get_config('SHOULD_STOP')
        should_stop_id_override = ReliableExecutor.s_config_manager.get_config_read('SHOULD_STOP_PRODUCER')[str(self.id)]

        if should_stop_id_override is not None:
            return should_stop_id_override
        
        return should_stop

    def insert_ads_into_database(self, urls, current_url):
        self.log_message_with_timestamp("Starting insert relative url into database for:{0}".format(current_url))
        for url in urls:
            self.database.insert_using_template_command('insert_ad_url', { 'ad_url': url, 'extracted': False})
        self.log_message_with_timestamp("Finished insert relative url into database for:{0}".format(current_url))

    def get_ad_urls(self):
        found_key = False

        for url_key, url_value in self.urls.items():

            if self.current_ad_base_url_key is not None and self.current_ad_base_url_key == url_key:
                found_key = True
                self.log_message_with_timestamp('Found ad key. Starting to get page number')
                self.page = self.get_page_number()
                self.log_message_with_timestamp('Got page number:{0}')
            elif not found_key:
                continue
            
            self.log_message_with_timestamp('Getting all ads for:{0}'.format(url_key))
            while self.page < ReliableExecutor.s_config_manager.get_config('PRODUCER_PAGE_LIMIT'):
                
                ### test whether we should finish work
                if self.should_stop():
                    self.log_message_with_timestamp('Stopping')
                    return

                msg = "Current page for '{0}' is {1}".format(url_key, self.page)
                self.log_message_with_timestamp(msg)

                current_url = url_value + '?page={0}'.format(self.page)
                response_text = self.execute(current_url)
                
                if response_text is None:
                    continue
                
                urls = self.extract_urls(response_text)

                if urls is None:
                    continue
                
                # If there are no more urls to process break
                if len(urls) == 0:
                    break

                self.log_message_with_timestamp('Starting insert ad urls for:{0}'.format(current_url))
                self.insert_ads(urls)
                self.log_message_with_timestamp('Finished insert ad urls for:{0}'.format(current_url))

                # insert relative urls into database table ad_url so that later
                # there is information of urls we failed to fetch
                # and avoid using producers, instead use just consumers
                # TODO: Add method to flag the ad as extracted
                # TODO: Before using database as source of urls make sure that:
                # 1. We have marked all extracted urls as extracted
                # 2. We have support if url is not available anymore
                # 3. We add support so that each time url is extracted it is marked as extracted (some trigger maybe)
                self.insert_ads_into_database(urls, current_url)

                # sleep a little bit so we don't overload the server
                sleep_time = random.randint(2,4)
                self.log_message_with_timestamp('Sleeping for {0} seconds'.format(sleep_time))
                time.sleep(sleep_time)

                #ad_type, real_estate_type = url_key.split('-')

                # go to the next page
                self.page = self.get_page_number()

            # wait for other producers to come here
            self.log_message_with_timestamp('Page:{0} is greater than limit:{1}'.format(self.page, ReliableExecutor.s_config_manager.get_config('PRODUCER_PAGE_LIMIT')))
            self.log_message_with_timestamp('Page limit barrier')
            Producer.s_barrier.wait()
            self.log_message_with_timestamp('Past page limit barrier')
            Producer.reset_page_number()
            self.page = self.get_page_number()
            Producer.s_barrier.wait()
            self.log_message_with_timestamp('Past page limit reset barrier')

    def run(self):
        self.get_ad_urls()