import logging
import threading
from datetime import datetime
import json

class ConfigManager:

    s_config_manager = None
    s_last_refreshed = None

    def __init__(self):
        self.config = None
        ConfigManager.s_last_refreshed = datetime.now()
        self.__load()
    
    def __load(self):
        logging.log(logging.DEBUG, 'Refreshing config')
        try:
            with open('config.json', 'r') as cfg:
                self.config = json.loads(cfg.read())
                ConfigManager.s_last_refreshed = datetime.now()
        except Exception as e:
            logging.log(logging.ERROR, e.args)
    
    @staticmethod
    def get_config_manager():
        if ConfigManager.s_config_manager is None:
           ConfigManager.s_config_manager = ConfigManager()

        return ConfigManager.s_config_manager 

    def get_config(self, config_name):
        if abs((ConfigManager.s_last_refreshed - datetime.now()).total_seconds() / 60) > self.config['CONFIG_REFRESH_TIME_IN_MINUTES']:
            ConfigManager.s_last_refreshed = datetime.now()
            self.__load()

        return self.get_config_read(config_name)

    def get_config_read(self, config_name):
        if config_name in self.config:
            return self.config[config_name]
        
        return None