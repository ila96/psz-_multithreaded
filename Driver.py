from Producer import Producer
from Consumer import Consumer
from ConfigManager import ConfigManager

cfg = ConfigManager.get_config_manager()

producers = []
consumers = []

for i in range(0, cfg.get_config_read('NUMBER_OF_PRODUCERS')):
    producer = Producer(i)
    producers.append(producer)
    print('Starting producer:{0}'.format(i))
    producer.start()

for i in range(0, cfg.get_config_read('NUMBER_OF_CONSUMERS')):
    consumer = Consumer(i)
    consumers.append(consumer)
    print('Starting consumer:{0}'.format(i))
    consumer.start()

for th in (producers + consumers):
    th.join()